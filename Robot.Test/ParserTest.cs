﻿using Robot.Core;
using Robot.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Robot.Test
{
    public class ParserTest
    {
        private readonly FileIO fileIO;

        public ParserTest()
        {
            fileIO = new FileIO();
        }

        private List<Command> GetCommands(string validCommandText)
        {
            List<Command> cmds = new List<Command>();
            try
            {
                // Create a temporary file
                string filename = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constant.ROBOT_TEMPORARY_FILE;
                File.WriteAllText(filename, validCommandText);

                cmds = fileIO.GetCommandsInFile(filename);

                // Delete the temporary file
                File.Delete(filename);
            }
            catch (Exception ex)
            {
                Logging.LogError(ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
            }
            return cmds;
        }

        [Fact]
        public void Given_valid_PLACE_command_text_then_valid_PLACE_command_is_generated()
        {
            // PLACE 0,0,NORTH
            string validPlaceText = EnumCommand.PLACE.ToString() + Constant.COMMAND_SPACE_SEPERATOR +
                Constant.MIN_SQUARE_X + Constant.COMMAND_COMMA_SEPERATOR +
                Constant.MIN_SQUARE_Y + Constant.COMMAND_COMMA_SEPERATOR +
                EnumDirection.NORTH.ToString();
            List<Command> commands = GetCommands(validPlaceText);

            Assert.NotEmpty(commands);
            Assert.Equal(typeof(Place), commands[0].GetType());
        }

        [Fact]
        public void Given_valid_MOVE_command_text_then_valid_MOVE_command_is_generated()
        {
            // MOVE
            string validMoveText = EnumCommand.MOVE.ToString();
            List<Command> commands = GetCommands(validMoveText);

            Assert.NotEmpty(commands);
            Assert.Equal(typeof(Move), commands[0].GetType());
        }

        [Fact]
        public void Given_valid_LEFT_command_text_then_valid_LEFT_command_is_generated()
        {
            // LEFT
            string validLeftText = EnumCommand.LEFT.ToString();
            List<Command> commands = GetCommands(validLeftText);

            Assert.NotEmpty(commands);
            Assert.Equal(typeof(Left), commands[0].GetType());
        }

        [Fact]
        public void Given_valid_RIGHT_command_text_then_valid_RIGHT_command_is_generated()
        {
            // RIGHT
            string validRightText = EnumCommand.RIGHT.ToString();
            List<Command> commands = GetCommands(validRightText);

            Assert.NotEmpty(commands);
            Assert.Equal(typeof(Right), commands[0].GetType());
        }

        [Fact]
        public void Given_valid_REPORT_command_text_then_valid_REPORT_command_is_generated()
        {
            string validReportText = EnumCommand.REPORT.ToString();
            List<Command> commands = GetCommands(validReportText);

            Assert.NotEmpty(commands);
            Assert.Equal(typeof(Report), commands[0].GetType());
        }

        [Fact]
        public void Given_invalid_PLACE_command_text_then_no_command_is_generated()
        {
            // PLACE 0,NORTH,0
            string invalidPlaceText = "PLACE 0,6,0";
            List<Command> commands = GetCommands(invalidPlaceText);

            Assert.Empty(commands);
        }

        [Fact]
        public void Given_invalid_MOVE_command_text_then_no_command_is_generated()
        {
            string invalidMoveText = " MOVE";
            List<Command> commands = GetCommands(invalidMoveText);

            Assert.Empty(commands);
        }

        [Fact]
        public void Given_invalid_LEFT_command_text_then_no_command_is_generated()
        {
            string invalidLeftText = "left";
            List<Command> commands = GetCommands(invalidLeftText);

            Assert.Empty(commands);
        }

        [Fact]
        public void Given_invalid_RIGHT_command_text_then_no_command_is_generated()
        {
            string invalidRightText = "RIGHT ";
            List<Command> commands = GetCommands(invalidRightText);

            Assert.Empty(commands);
        }

        [Fact]
        public void Given_invalid_REPORT_command_text_then_no_command_is_generated()
        {
            string invalidReportText = "REPORt";
            List<Command> commands = GetCommands(invalidReportText);

            Assert.Empty(commands);
        }
    }
}
