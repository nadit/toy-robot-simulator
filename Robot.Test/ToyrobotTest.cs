﻿using Robot.Core;
using Robot.Utility;
using System.Collections.Generic;
using Xunit;

namespace Robot.Test
{
    public class ToyrobotTest
    {
        [Fact]
        public void Given_valid_commands_then_commands_can_be_executed()
        {
            List<Command> commands = new List<Command>();
            CommandPlace validPlace = new CommandPlace()
            {
                Direction = new East(),
                Position = new Point()
                {
                    X = Constant.MIN_SQUARE_X,
                    Y = Constant.MIN_SQUARE_Y
                }
            };
            Command cmdPlace = new Place(validPlace);
            Command cmdMove = new Move();
            Command cmdReport = new Report();
            commands.Add(cmdPlace);
            commands.Add(cmdMove);
            commands.Add(cmdReport);
            Toyrobot toyrobot = new Toyrobot();
            toyrobot.ExecuteCommands(commands);

            Assert.NotEmpty(toyrobot.GetMessages());
        }

        [Fact]
        public void Given_invalid_commands_then_commands_are_ignored()
        {
            List<Command> commands = new List<Command>();
            Command cmdMove = new Move();
            Command cmdReport = new Report();
            commands.Add(cmdMove);
            commands.Add(cmdReport);
            Toyrobot toyrobot = new Toyrobot();
            toyrobot.ExecuteCommands(commands);

            Assert.Empty(toyrobot.GetMessages());
        }
    }
}
