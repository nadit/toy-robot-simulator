using Robot.Core;
using Xunit;

namespace Robot.Test
{
    public class DirectionTest
    {
        [Fact]
        public void Given_the_direction_is_north_and_LEFT_command_is_executed_then_the_direction_is_west()
        {
            Direction north = new North();
            Assert.Equal(typeof(West), north.Left().GetType());
            Assert.NotEqual(typeof(East), north.Left().GetType());
            Assert.NotEqual(typeof(North), north.Left().GetType());
            Assert.NotEqual(typeof(South), north.Left().GetType());
        }

        [Fact]
        public void Given_the_direction_is_west_and_LEFT_command_is_executed_then_the_direction_is_south()
        {
            Direction west = new West();
            Assert.Equal(typeof(South), west.Left().GetType());
            Assert.NotEqual(typeof(East), west.Left().GetType());
            Assert.NotEqual(typeof(North), west.Left().GetType());
            Assert.NotEqual(typeof(West), west.Left().GetType());
        }

        [Fact]
        public void Given_the_direction_is_south_and_LEFT_command_is_executed_then_the_direction_is_east()
        {
            Direction south = new South();
            Assert.Equal(typeof(East), south.Left().GetType());
            Assert.NotEqual(typeof(South), south.Left().GetType());
            Assert.NotEqual(typeof(North), south.Left().GetType());
            Assert.NotEqual(typeof(West), south.Left().GetType());
        }

        [Fact]
        public void Given_the_direction_is_east_and_LEFT_command_is_executed_then_the_direction_is_north()
        {
            Direction east = new East();
            Assert.Equal(typeof(North), east.Left().GetType());
            Assert.NotEqual(typeof(South), east.Left().GetType());
            Assert.NotEqual(typeof(East), east.Left().GetType());
            Assert.NotEqual(typeof(West), east.Left().GetType());
        }

        [Fact]
        public void Given_the_direction_is_north_and_RIGHT_command_is_executed_then_the_direction_is_east()
        {
            Direction north = new North();
            Assert.Equal(typeof(East), north.Right().GetType());
            Assert.NotEqual(typeof(South), north.Right().GetType());
            Assert.NotEqual(typeof(North), north.Right().GetType());
            Assert.NotEqual(typeof(West), north.Right().GetType());
        }

        [Fact]
        public void Given_the_direction_is_east_and_RIGHT_command_is_executed_then_the_direction_is_south()
        {
            Direction east = new East();
            Assert.Equal(typeof(South), east.Right().GetType());
            Assert.NotEqual(typeof(East), east.Right().GetType());
            Assert.NotEqual(typeof(North), east.Right().GetType());
            Assert.NotEqual(typeof(West), east.Right().GetType());
        }

        [Fact]
        public void Given_the_direction_is_south_and_RIGHT_command_is_executed_then_the_direction_is_west()
        {
            Direction south = new South();
            Assert.Equal(typeof(West), south.Right().GetType());
            Assert.NotEqual(typeof(East), south.Right().GetType());
            Assert.NotEqual(typeof(North), south.Right().GetType());
            Assert.NotEqual(typeof(South), south.Right().GetType());
        }

        [Fact]
        public void Given_the_direction_is_west_and_RIGHT_command_is_executed_then_the_direction_is_north()
        {
            Direction west = new West();
            Assert.Equal(typeof(North), west.Right().GetType());
            Assert.NotEqual(typeof(East), west.Right().GetType());
            Assert.NotEqual(typeof(West), west.Right().GetType());
            Assert.NotEqual(typeof(South), west.Right().GetType());
        }
    }
}
