﻿using Robot.Core;
using Robot.Utility;
using Xunit;

namespace Robot.Test
{
    public class CommandTest
    {
        [Fact]
        public void Given_the_current_position_is_invalid_and_LEFT_command_is_executed_then_the_output_is_IGNORED_text_and_the_direction_remains_the_same()
        {
            Direction direction = new East();
            Point position = new Point() { X = Constant.MAX_SQUARE_X + 1, Y = Constant.MAX_SQUARE_Y + 1 };
            Command leftCmd = new Left();
            string output = leftCmd.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(typeof(East), direction.GetType());
            Assert.Equal(Constant.MAX_SQUARE_X + 1, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y + 1, position.Y);
        }

        [Fact]
        public void Given_the_current_position_is_valid_and_LEFT_command_is_executed_then_the_direction_is_updated()
        {
            Direction direction = new East();
            Point position = new Point() { X = Constant.MAX_SQUARE_X - 1, Y = Constant.MAX_SQUARE_Y - 1 };
            Command leftCmd = new Left();
            string output = leftCmd.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(string.Empty, output);
            Assert.Equal(typeof(North), direction.GetType());
        }

        [Fact]
        public void Given_the_current_position_is_invalid_and_RIGHT_command_is_executed_then_the_output_is_IGNORED_text_and_the_direction_remains_the_same()
        {
            Direction direction = new East();
            Point position = new Point() { X = Constant.MAX_SQUARE_X + 1, Y = Constant.MAX_SQUARE_Y + 1 };
            Command rightCmd = new Right();
            string output = rightCmd.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(typeof(East), direction.GetType());
            Assert.Equal(Constant.MAX_SQUARE_X + 1, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y + 1, position.Y);
        }

        [Fact]
        public void Given_the_current_position_is_valid_and_RIGHT_command_is_executed_then_the_direction_is_updated()
        {
            Direction direction = new East();
            Point position = new Point() { X = Constant.MAX_SQUARE_X - 1, Y = Constant.MAX_SQUARE_Y - 1 };
            Command leftCmd = new Right();
            string output = leftCmd.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(string.Empty, output);
            Assert.Equal(typeof(South), direction.GetType());
        }

        [Fact]
        public void Given_the_direction_is_west_and_MOVE_command_is_executed_and_the_new_position_is_valid_then_the_current_position_is_updated()
        {
            Point position = new Point() { X = 2, Y = 2 };
            Direction direction = new West();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(1, position.X);
            Assert.Equal(2, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_east_and_MOVE_command_is_executed_and_the_new_position_is_valid_then_the_current_position_is_updated()
        {
            Point position = new Point() { X = 2, Y = 2 };
            Direction direction = new East();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(3, position.X);
            Assert.Equal(2, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_south_and_MOVE_command_is_executed_and_the_new_position_is_valid_then_the_current_position_is_updated()
        {
            Point position = new Point() { X = 2, Y = 2 };
            Direction direction = new South();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(2, position.X);
            Assert.Equal(1, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_north_and_MOVE_command_is_executed_and_the_new_position_is_valid_then_the_current_position_is_updated()
        {
            Point position = new Point() { X = 2, Y = 2 };
            Direction direction = new North();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(2, position.X);
            Assert.Equal(3, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_north_and_MOVE_command_is_executed_and_the_current_position_is_in_boundary_then_the_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MIN_SQUARE_X, Y = Constant.MAX_SQUARE_Y };
            Direction direction = new North();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MIN_SQUARE_X, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_south_and_MOVE_command_is_executed_and_the_current_position_is_in_boundary_then_the_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MIN_SQUARE_X, Y = Constant.MIN_SQUARE_Y };
            Direction direction = new South();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MIN_SQUARE_X, position.X);
            Assert.Equal(Constant.MIN_SQUARE_Y, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_west_and_MOVE_command_is_executed_and_the_current_position_is_in_boundary_then_the_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MIN_SQUARE_X, Y = Constant.MIN_SQUARE_Y };
            Direction direction = new West();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MIN_SQUARE_X, position.X);
            Assert.Equal(Constant.MIN_SQUARE_Y, position.Y);
        }

        [Fact]
        public void Given_the_direction_is_east_and_MOVE_command_is_executed_and_the_current_position_is_in_boundary_then_the_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MAX_SQUARE_X, Y = Constant.MIN_SQUARE_Y };
            Direction direction = new East();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MAX_SQUARE_X, position.X);
            Assert.Equal(Constant.MIN_SQUARE_Y, position.Y);
        }

        [Fact]
        public void Given_the_current_position_is_invalid_and_MOVE_command_is_executed_then_the_output_is_IGNORED_and_the_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MAX_SQUARE_X + 1, Y = Constant.MAX_SQUARE_Y + 1 };
            Direction direction = new North();
            Command cmdMove = new Move();
            string output = cmdMove.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.NotEqual(string.Empty, output);
            Assert.Equal(Constant.MAX_SQUARE_X + 1, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y + 1, position.Y);
        }

        [Fact]
        public void Given_the_current_position_is_valid_and_REPORT_command_is_executed_then_the_output_is_not_IGNORED_text_and_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MAX_SQUARE_X - 1, Y = Constant.MAX_SQUARE_Y - 1 };
            Direction direction = new North();
            Command cmdReport = new Report();
            string output = cmdReport.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.NotEqual(string.Empty, output);
            Assert.Equal(Constant.MAX_SQUARE_X - 1, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y - 1, position.Y);
        }

        [Fact]
        public void Given_the_current_position_is_invalid_and_REPORT_command_is_executed_then_the_output_is_IGNORED_text_and_current_position_remains_the_same()
        {
            Point position = new Point() { X = Constant.MAX_SQUARE_X + 1, Y = Constant.MAX_SQUARE_Y + 1 };
            Direction direction = new North();
            Command cmdReport = new Report();
            string output = cmdReport.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MAX_SQUARE_X + 1, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y + 1, position.Y);
        }

        [Fact]
        public void Given_valid_PLACE_command_then_the_command_can_be_executed_and_the_current_position_and_direction_are_updated()
        {
            Point position = new Point() { X = Constant.MIN_SQUARE_X, Y = Constant.MIN_SQUARE_Y };
            Direction direction = new North();
            CommandPlace validPlace = new CommandPlace()
            {
                Direction = new East(),
                Position = new Point()
                {
                    X = Constant.MAX_SQUARE_X,
                    Y = Constant.MAX_SQUARE_Y
                }
            };
            Command cmd = new Place(validPlace);
            string output = cmd.Execute(ref position, ref direction);

            Assert.NotEqual(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MAX_SQUARE_X, position.X);
            Assert.Equal(Constant.MAX_SQUARE_Y, position.Y);
            Assert.Equal(typeof(East), direction.GetType());
        }

        [Fact]
        public void Given_invalid_PLACE_command_then_the_command_is_IGNORED_and_the_current_position_and_direction_remain_the_same()
        {
            Point position = new Point() { X = Constant.MIN_SQUARE_X, Y = Constant.MIN_SQUARE_Y };
            Direction direction = new North();
            CommandPlace invalidPlace = new CommandPlace()
            {
                Direction = new East(),
                Position = new Point()
                {
                    X = Constant.MAX_SQUARE_X + 1,
                    Y = Constant.MAX_SQUARE_Y + 1
                }
            };
            Command cmd = new Place(invalidPlace);
            string output = cmd.Execute(ref position, ref direction);

            Assert.Equal(Constant.COMMAND_IGNORED, output);
            Assert.Equal(Constant.MIN_SQUARE_X, position.X);
            Assert.Equal(Constant.MIN_SQUARE_Y, position.Y);
            Assert.Equal(typeof(North), direction.GetType());
        }
    }
}
