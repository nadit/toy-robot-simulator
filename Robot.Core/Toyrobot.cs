﻿using Robot.Utility;
using System.Collections.Generic;

namespace Robot.Core
{
    public class Toyrobot
    {
        private Direction currentDirection;
        private Point currentPosition;
        private List<string> messages;
        private bool isCommandsValid;

        /// <summary>
        /// Get direction object from a direction enum
        /// </summary>
        /// <param name="enumDir">an enum of direction</param>
        /// <returns>direction object</returns>
        private Direction GetDirection(EnumDirection enumDir)
        {
            if (enumDir == EnumDirection.EAST)
            {
                return new East();
            }
            else if (enumDir == EnumDirection.NORTH)
            {
                return new North();
            }
            else if (enumDir == EnumDirection.SOUTH)
            {
                return new South();
            }
            else if (enumDir == EnumDirection.WEST)
            {
                return new West();
            }
            else
            {
                return new East();
            }
        }

        /// <summary>
        /// Initialize Toyrobot object with initial/default values
        /// </summary>
        public Toyrobot()
        {
            // Initialize default position
            currentPosition = new Point() { X = Constant.ROBOT_INIT_X, Y = Constant.ROBOT_INIT_Y };
            currentDirection = GetDirection(Constant.ROBOT_INIT_DIRECTION);
            messages = new List<string>();
            isCommandsValid = false;
        }

        /// <summary>
        /// Execute commands in the robot, a list of commands is valid when the first command is a PLACE command.
        /// It will discard all messages (of the previous commands) when a valid PLACE command is found.
        /// </summary>
        /// <param name="cmds"></param>
        public void ExecuteCommands(List<Command> cmds)
        {
            isCommandsValid = false;
            foreach (Command cmd in cmds)
            {
                // Discard messages when a command is a PLACE command
                if (cmd.GetType() == typeof(Place))
                {
                    if (cmd.IsPositionValid(((Place)cmd).GetPositionInCommand()))
                    {
                        isCommandsValid = true;
                    }
                }
                if (isCommandsValid)
                {
                    string msg = cmd.Execute(ref currentPosition, ref currentDirection);
                    if (msg.IndexOf(Constant.COMMAND_OUTPUT) == 0)
                    {
                        messages.Add(msg);
                    }
                }
            }
            isCommandsValid = false;
        }

        /// <summary>
        /// Get all messages from the executed commands
        /// </summary>
        /// <returns>a list of messages</returns>
        public List<string> GetMessages()
        {
            return messages;
        }
    }
}