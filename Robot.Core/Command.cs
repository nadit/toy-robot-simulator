﻿using Robot.Utility;

namespace Robot.Core
{
    public abstract class Command
    {
        /// <summary>
        /// Execute the command. It depends on the type of command.
        /// Given the condition to execute the command is not satisfied, then it will return IGNORED text
        /// </summary>
        /// <param name="currentPosition">the current position. After executing the command, the current position is updated</param>
        /// <param name="currentDirection">the current direction. After executing the command, the current direction is updated</param>
        /// <returns>a text message to represent the executed command</returns>
        public abstract string Execute(ref Point currentPosition, ref Direction currentDirection);

        /// <summary>
        /// To check whether the new position is valid or not
        /// </summary>
        /// <param name="position">the position</param>
        /// <returns>True when valid, otherwise False</returns>
        public bool IsPositionValid(Point position)
        {
            if (position != null)
            {
                if (position.X >= Constant.MIN_SQUARE_X &&
                    position.X <= Constant.MAX_SQUARE_X &&
                    position.Y >= Constant.MIN_SQUARE_Y &&
                    position.Y <= Constant.MAX_SQUARE_Y)
                {
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// An object of a command PLACE containing the requested position (X and Y) and the requested direction
    /// </summary>
    public class CommandPlace
    {
        public Point Position { get; set; }
        public Direction Direction { get; set; }
    }

    public class Place : Command
    {
        private CommandPlace cmdPlace;

        /// <summary>
        /// Get command PLACE
        /// </summary>
        /// <param name="cmdPlace">an object containing information about the requested position (X and Y) and the requested direction</param>
        /// <returns>a string with the following format: PLACE[space]X[comma]Y[comma]Direction</returns>
        private string GetMessage(CommandPlace cmdPlace)
        {
            return EnumCommand.PLACE.ToString() + Constant.COMMAND_SPACE_SEPERATOR +
                cmdPlace.Position.X + Constant.COMMAND_COMMA_SEPERATOR +
                cmdPlace.Position.Y + Constant.COMMAND_COMMA_SEPERATOR +
                cmdPlace.Direction.ToString();
        }

        /// <summary>
        /// Constructor with a object of a command PLACE
        /// </summary>
        /// <param name="cmdPlace"></param>
        public Place(CommandPlace cmdPlace)
        {
            this.cmdPlace = cmdPlace;
        }

        /// <summary>
        /// Execute the PLACE command iff the given position is valid. 
        /// PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.
        /// If the given position is invalid then return IGNORED text.
        /// </summary>
        /// <param name="currentPosition">the current position. After executing the command, the current position is updated iff the new position is valid</param>
        /// <param name="currentDirection">the current direction. After executing the command, the current direction is updated iff the new position is valid</param>
        /// <returns>a text message to represent the executed command</returns>
        public override string Execute(ref Point currentPosition, ref Direction currentDirection)
        {
            string retVal = Constant.COMMAND_IGNORED;
            // PLACE Command
            if (IsPositionValid(cmdPlace.Position))
            {
                currentPosition = cmdPlace.Position;
                currentDirection = cmdPlace.Direction;
                retVal = string.Empty;
            }
            return retVal;
        }

        /// <summary>
        /// Get the position in PLACE command that it is approaching
        /// </summary>
        /// <returns>the position in PLACE command</returns>
        public Point GetPositionInCommand()
        {
            return cmdPlace.Position;
        }
    }

    public class Move : Command
    {
        /// <summary>
        /// Execute the MOVE command if the current position is valid. 
        /// MOVE will move one unit forward following the current direction.
        /// Given the current position is invalid then return IGNORED text.
        /// </summary>
        /// <param name="currentPosition">the current position. After executing the command, the current position is updated iff the new position is valid</param>
        /// <param name="currentDirection">the current direction.</param>
        /// <returns>a text message to represent the executed command</returns>
        public override string Execute(ref Point currentPosition, ref Direction currentDirection)
        {
            string retVal = Constant.COMMAND_IGNORED;
            // MOVE Command
            Point newPos = CalculateNewPosition(currentPosition, currentDirection);
            if (IsPositionValid(newPos))
            {
                currentPosition = newPos;
                retVal = string.Empty;
            }
            return retVal;
        }

        /// <summary>
        /// To calculate new position with regard to the current position and the current direction
        /// </summary>
        /// <param name="currentPosition">the current position</param>
        /// <param name="currentDirection">the current direction</param>
        /// <returns>a new position after movement</returns>
        private Point CalculateNewPosition(Point currentPosition, Direction currentDirection)
        {
            Point newPos = currentDirection.Move(currentPosition);
            return newPos;
        }
    }

    public class Left : Command
    {
        /// <summary>
        /// Execute the LEFT command iff the current position is valid. 
        /// LEFT will rotate 90 degrees in the left direction without changing the current position.
        /// Given the current position is invalid then return IGNORED text.
        /// </summary>
        /// <param name="currentPosition">the current position.</param>
        /// <param name="currentDirection">the current direction. After executing the command, the current direction is updated</param>
        /// <returns>a text message to represent the executed command</returns>
        public override string Execute(ref Point currentPosition, ref Direction currentDirection)
        {
            string retVal = Constant.COMMAND_IGNORED;
            // LEFT Command
            // A counter-clockwise direction
            if (IsPositionValid(currentPosition))
            {
                currentDirection = currentDirection.Left();
                retVal = string.Empty;
            }
            return retVal;
        }
    }

    public class Right : Command
    {
        /// <summary>
        /// Execute the RIGHT command iff the current position is valid. 
        /// RIGHT will rotate 90 degrees in the right direction without changing the current position.
        /// Given the current position is invalid then return IGNORED text.
        /// </summary>
        /// <param name="currentPosition">the current position.</param>
        /// <param name="currentDirection">the current direction. After executing the command, the current direction is updated</param>
        /// <returns>a text message to represent the executed command</returns>
        public override string Execute(ref Point currentPosition, ref Direction currentDirection)
        {
            string retVal = Constant.COMMAND_IGNORED;
            // RIGHT Command
            // A clockwise direction
            if (IsPositionValid(currentPosition))
            {
                currentDirection = currentDirection.Right();
                retVal = string.Empty;
            }
            return retVal;
        }
    }

    public class Report : Command
    {
        /// <summary>
        /// Execute the REPORT command iff the current position is valid. 
        /// REPORT will announce the X,Y and direction in a standard output.
        /// Given the current position is invalid then return IGNORED text.
        /// </summary>
        /// <param name="currentPosition">the current position.</param>
        /// <param name="currentDirection">the current direction.</param>
        /// <returns>a text message to represent the executed command and the X, Y, direction of the robot</returns>
        public override string Execute(ref Point currentPosition, ref Direction currentDirection)
        {
            string retVal = Constant.COMMAND_IGNORED;
            if (IsPositionValid(currentPosition))
            {
                // REPORT Command
                retVal = Constant.COMMAND_OUTPUT + Constant.COMMAND_SPACE_SEPERATOR +
                    currentPosition.X + Constant.COMMAND_COMMA_SEPERATOR +
                    currentPosition.Y + Constant.COMMAND_COMMA_SEPERATOR +
                    currentDirection.ToString();
            }
            return retVal;
        }
    }
}