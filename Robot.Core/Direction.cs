﻿using Robot.Utility;

namespace Robot.Core
{
    public abstract class Direction
    {
        /// <summary>
        /// Rotate 90 degrees to the left without changing the position
        /// </summary>
        /// <returns>A new direction</returns>
        public abstract Direction Left();

        /// <summary>
        /// Rotate 90 degrees to the right without changing the position
        /// </summary>
        /// <returns>A new direction</returns>
        public abstract Direction Right();

        /// <summary>
        /// Move one unit forward in the direction it is currently facing
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <returns></returns>
        public abstract Point Move(Point currentPosition);
    }

    /// <summary>
    /// Class to handle NORTH direction
    /// </summary>
    public class North : Direction
    {
        public override Direction Left()
        {
            return new West();
        }

        public override Direction Right()
        {
            return new East();
        }

        public override Point Move(Point currentPosition)
        {
            return new Point()
            {
                X = currentPosition.X,
                Y = currentPosition.Y + 1
            };
        }

        public override string ToString()
        {
            return EnumDirection.NORTH.ToString();
        }
    }

    /// <summary>
    /// Class to handle EAST direction
    /// </summary>
    public class East : Direction
    {
        public override Direction Left()
        {
            return new North();
        }

        public override Direction Right()
        {
            return new South();
        }

        public override Point Move(Point currentPosition)
        {
            return new Point()
            {
                X = currentPosition.X + 1,
                Y = currentPosition.Y
            };
        }

        public override string ToString()
        {
            return EnumDirection.EAST.ToString();
        }
    }

    /// <summary>
    /// Class to handle SOUTH direction
    /// </summary>
    public class South : Direction
    {
        public override Direction Left()
        {
            return new East();
        }

        public override Direction Right()
        {
            return new West();
        }

        public override Point Move(Point currentPosition)
        {
            return new Point()
            {
                X = currentPosition.X,
                Y = currentPosition.Y - 1
            };
        }

        public override string ToString()
        {
            return EnumDirection.SOUTH.ToString();
        }
    }

    /// <summary>
    /// Class to handle WEST direction
    /// </summary>
    public class West : Direction
    {
        public override Direction Left()
        {
            return new South();
        }

        public override Direction Right()
        {
            return new North();
        }

        public override Point Move(Point currentPosition)
        {
            return new Point()
            {
                X = currentPosition.X - 1,
                Y = currentPosition.Y
            };
        }

        public override string ToString()
        {
            return EnumDirection.WEST.ToString();
        }
    }
}
