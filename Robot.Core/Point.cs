﻿namespace Robot.Core
{
    /// <summary>
    /// Two dimentional representation of a robot's position
    /// </summary>
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
