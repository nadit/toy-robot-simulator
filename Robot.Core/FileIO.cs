﻿using Robot.Core;
using System;
using System.Collections.Generic;
using System.IO;

namespace Robot.Utility
{
    /// <summary>
    /// A class to handle input and output file operations
    /// </summary>
    public class FileIO
    {
        /// <summary>
        /// Get a list of files from a directory containing input files
        /// </summary>
        /// <param name="folderPath">directory path containing a list of input files</param>
        /// <returns>a list of input files (full path)</returns>
        public string[] GetInputFilesInFolder(string folderPath)
        {
            string[] inputFiles = new string[0];
            if (Directory.Exists(folderPath))
            {
                inputFiles = Directory.GetFiles(folderPath, "*" + Constant.ROBOT_INPUT_FILE_EXTENSION);
            }
            return inputFiles;
        }

        /// <summary>
        /// Write output files as the results of executing commands in a folder
        /// </summary>
        /// <param name="folderPath">a directory path for output files</param>
        /// <param name="messages">a list of messages to write</param>
        /// <param name="outputFilename">output filename</param>
        /// <returns>True if succeed, otherwise False</returns>
        public bool WriteOutputFilesInFolder(string folderPath, List<string> messages, string outputFilename)
        {
            bool isSucceed = false;
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                string filename = folderPath + Path.DirectorySeparatorChar + outputFilename;
                // If the file already exists, delete it
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
                // If the file does not exist, create it
                FileStream stream = File.Create(filename);
                using (StreamWriter file = new StreamWriter(stream))
                {
                    foreach (string line in messages)
                    {
                        file.WriteLine(line);
                    }
                }
                isSucceed = true;
            }
            catch (Exception ex)
            {
                // error in producing output file
                Logging.LogError(ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
            }
            return isSucceed;
        }

        /// <summary>
        /// Get a list of commands from a file input
        /// </summary>
        /// <param name="filePath">a location path of the file</param>
        /// <returns>a list of command objects</returns>
        public List<Command> GetCommandsInFile(string filePath)
        {
            List<Command> commands = new List<Command>();
            string[] lines = new string[0];
            if (File.Exists(filePath))
            {
                lines = File.ReadAllLines(filePath);
                foreach (string line in lines)
                {
                    try
                    {
                        if (Constant.COMMAND_PLACE_EXPRESSION.Match(line).Success)
                        {
                            AddCommandPlace(line, commands);
                        }
                        else if (Constant.COMMAND_MOVE_EXPRESSION.Match(line).Success)
                        {
                            commands.Add(new Move());
                        }
                        else if (Constant.COMMAND_LEFT_EXPRESSION.Match(line).Success)
                        {
                            commands.Add(new Left());
                        }
                        else if (Constant.COMMAND_RIGHT_EXPRESSION.Match(line).Success)
                        {
                            commands.Add(new Right());
                        }
                        else if (Constant.COMMAND_REPORT_EXPRESSION.Match(line).Success)
                        {
                            commands.Add(new Report());
                        }
                    }
                    catch (Exception ex)
                    {
                        // error in reading a command
                        Logging.LogError(ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
                    }
                }
            }
            return commands;
        }

        /// <summary>
        /// Add command with type PLACE to the list of commands. Using a regular expression to match the value of X, Y, and F
        /// </summary>
        /// <param name="line">a text line of command</param>
        /// <param name="cmds">a list of commands</param>
        private void AddCommandPlace(string line, List<Command> cmds)
        {
            try
            {
                var match = Constant.COMMAND_PLACE_EXPRESSION.Match(line);
                int x = int.Parse(match.Groups[Constant.COMMAND_PLACE_X].Value);
                int y = int.Parse(match.Groups[Constant.COMMAND_PLACE_Y].Value);
                EnumDirection enumDir = (EnumDirection)Enum.Parse(typeof(EnumDirection),
                    match.Groups[Constant.COMMAND_PLACE_F].Value);
                Direction direction = GetDirection(enumDir);

                cmds.Add(
                    new Place(
                        new CommandPlace()
                        {
                            Direction = direction,
                            Position = new Point()
                            {
                                X = x,
                                Y = y
                            }
                        }));
            }
            catch(Exception ex)
            {
                Logging.LogError(ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
            }
        }

        /// <summary>
        /// Get Direction object from an enum direction
        /// </summary>
        /// <param name="enumDir">enum direction</param>
        /// <returns>direction object that represents the enum direction</returns>
        private Direction GetDirection(EnumDirection enumDir)
        {
            Direction direction = null;
            switch (enumDir)
            {
                case EnumDirection.EAST:
                    direction = new East();
                    break;
                case EnumDirection.NORTH:
                    direction = new North();
                    break;
                case EnumDirection.SOUTH:
                    direction = new South();
                    break;
                case EnumDirection.WEST:
                    direction = new West();
                    break;
                default:
                    break;
            }
            return direction;
        }
    }
}
