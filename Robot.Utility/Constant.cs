﻿using System.Text.RegularExpressions;

namespace Robot.Utility
{
    public enum EnumDirection
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }

    public enum EnumCommand
    {
        PLACE,
        MOVE,
        LEFT,
        RIGHT,
        REPORT
    }

    /// <summary>
    /// Class containing definition of the application
    /// </summary>
    public static class Constant
    {
        // Definition of the table's size (index starts from 0)
        // A square table with dimensions 5 units x 5 units
        public static readonly int MIN_SQUARE_X = 0;
        public static readonly int MIN_SQUARE_Y = 0;
        public static readonly int MAX_SQUARE_X = 4;
        public static readonly int MAX_SQUARE_Y = 4;

        // Definition of the robot's initial position
        public static readonly int ROBOT_INIT_X = 0;
        public static readonly int ROBOT_INIT_Y = 0;
        public static readonly EnumDirection ROBOT_INIT_DIRECTION = EnumDirection.EAST;

        // Definition of the folder names
        public static readonly string ROBOT_INPUT_FOLDER = "Robot.Input";
        public static readonly string ROBOT_OUTPUT_FOLDER = "Robot.Output";
        public static readonly string ROBOT_LOG_FILE = "Robot.log";
        public static readonly string ROBOT_TEMPORARY_FILE = "Robot.temp";
        public static readonly string ROBOT_INPUT_FILE_EXTENSION = ".input";
        public static readonly string ROBOT_OUTPUT_FILE_EXTENSION = ".output";
        public static readonly string ROBOT_OUTPUT_FILENAME = "out";
        public static readonly string ROBOT_TEST_INPUT_FOLDER = "Robot.Test.Input";
        public static readonly string ROBOT_TEST_OUTPUT_FOLDER = "Robot.Test.Output";

        // Definition of the command names
        public static readonly char COMMAND_SPACE_SEPERATOR = ' ';
        public static readonly char COMMAND_COMMA_SEPERATOR = ',';
        public static readonly string COMMAND_OUTPUT = "Output:";
        public static readonly string COMMAND_IGNORED = "Ignored";

        // Definition of the command expressions
        public static readonly Regex COMMAND_PLACE_EXPRESSION = new Regex(@"^PLACE (?<X>[0-9]+),(?<Y>[0-9]+),(?<F>NORTH|EAST|SOUTH|WEST)$");
        public static readonly string COMMAND_PLACE_X = "X";
        public static readonly string COMMAND_PLACE_Y = "Y";
        public static readonly string COMMAND_PLACE_F = "F";
        public static readonly Regex COMMAND_MOVE_EXPRESSION = new Regex(@"^MOVE$");
        public static readonly Regex COMMAND_LEFT_EXPRESSION = new Regex(@"^LEFT$");
        public static readonly Regex COMMAND_RIGHT_EXPRESSION = new Regex(@"^RIGHT$");
        public static readonly Regex COMMAND_REPORT_EXPRESSION = new Regex(@"^REPORT$");
    }
}
