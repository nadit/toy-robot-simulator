﻿using System;
using System.IO;
using System.Diagnostics;

namespace Robot.Utility
{
    public static class Logging
    {
        /// <summary>
        /// Generate a log file (if it does not exist) and append error message
        /// </summary>
        /// <param name="errMsg">text containing an error message</param>
        public static void LogError(string errMsg)
        {
            try
            {
                errMsg = DateTime.Now.ToString() + Constant.COMMAND_SPACE_SEPERATOR + errMsg + Environment.NewLine;
                string logfile = Directory.GetCurrentDirectory() +
                    Path.DirectorySeparatorChar + Constant.ROBOT_LOG_FILE;
                File.AppendAllText(logfile, errMsg);
            }
            catch (ArgumentException ex)
            {
                Debug.WriteLine(ex.GetType().ToString() + ex.Message);
            }
            catch (PathTooLongException ex)
            {
                Debug.WriteLine(ex.GetType().ToString() + ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                Debug.WriteLine(ex.GetType().ToString() + ex.Message);
            }
            catch (IOException ex)
            {
                Debug.WriteLine(ex.GetType().ToString() + ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                Debug.WriteLine(ex.GetType().ToString() + ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.GetType().ToString() + ex.Message);
            }
        }
    }
}
