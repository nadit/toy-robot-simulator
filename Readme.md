# Toy robot simulator
This application is developed using .NET Core 2.0. 
If you have not installed the .NET Core, please install it first in order to build/execute the application. 
The .NET Core SDK can be installed on Mac, Linux, and Windows.
Please visit https://www.microsoft.com/net/core for more information.

# Build the projects

To build the projects, from the `Robot` folder, run commands below from your Shell/Bash:

`cd Robot`

`dotnet build Robot.Utility`

`dotnet build Robot.Core`

`dotnet build Robot.Simulator`

`dotnet build Robot.Test`

# Run the toy robot simulator

The program can operate by receiving input from file(s) located in `Robot.Input` folder.

The application will read input files from `Robot.Input` folder and put the output files as the results in `Robot.Output` folder.
An input file contains a list of commands for the toy robot. After executing commands from an input file, the robot will produce an output file.
The output file might (not) contain any output text depending on whether the commands are valid or not.

go to Robot.Simulator folder

`cd Robot.Simulator`

run the program

`dotnet run`

check the results/output files in `Robot.Output` folder.
A file name in input folder relates to the same file name in output folder with different extension.
For example, a file "cmd0.input" will have the related output file of "cmd0.output".

# Robot.Input files

+ cmd0.input - to demonstrate the MOVE command

+ cmd1.input - to demonstrate the LEFT command

+ cmd2.input - to demonstrate the movement and rotation

+ cmd3.input - to demonstrate invalid commands (without PLACE)

+ cmd4.input - to demonstrate invalid PLACE command

+ cmd5.input - to demonstrate multiple PLACE commands combined with the movement and rotation

+ cmd6.input - to demonstrate a robot that is not on the table (invalid PLACE command)

+ cmd7.input - to demonstrate invalid commands

+ cmd8.input - to demonstrate commands without a REPORT command

+ cmd9.input - to demonstrate an invalid REPORT command

+ cmd10.input - to demonstrate an invalid command

+ cmd11.input - to demonstrate the prevention of the robot to fall from the table (NORTH direction)

+ cmd12.input - to demonstrate the prevention of the robot to fall from the table (SOUTH direction)

+ cmd13.input - to demonstrate the prevention of the robot to fall from the table (WEST direction)

+ cmd14.input - to demonstrate the prevention of the robot to fall from the table (EAST direction)

+ cmd15.input - to demonstrate multiple PLACE commands with an invalid PLACE command in between

# Run the unit testing

There are 37 unit tests created based on the requirements. The source code in the unit tests should be able to explain by itself.

To execute the unit testing, go to Robot.Test folder

`cd Robot.Test`

and run xunit

`dotnet xunit`

alternatively, to run the unit testing, this command would also work

`dotnet test`