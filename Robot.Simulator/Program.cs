﻿using Robot.Core;
using Robot.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Robot.Simulator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get Robot.Input folder path
            string currentDir = Directory.GetCurrentDirectory();
            DirectoryInfo parrentDir = Directory.GetParent(currentDir);
            string robotInputPath = parrentDir.FullName + Path.DirectorySeparatorChar + Constant.ROBOT_INPUT_FOLDER;
            string robotOutputPath = parrentDir.FullName + Path.DirectorySeparatorChar + Constant.ROBOT_OUTPUT_FOLDER;
            FileIO fileIO = new FileIO();
            string[] inputFiles = fileIO.GetInputFilesInFolder(robotInputPath);

            // Read every input file and populate commands
            foreach (string inputFile in inputFiles)
            {
                // Initialize a toy robot simulator
                Toyrobot toyrobot = new Toyrobot();

                List<Command> commands = fileIO.GetCommandsInFile(inputFile);

                string filename = Path.GetFileNameWithoutExtension(inputFile);

                // Execute commands
                toyrobot.ExecuteCommands(commands);

                // Collect the output/trace and save them in Robot.Output folder
                fileIO.WriteOutputFilesInFolder(robotOutputPath, toyrobot.GetMessages(),
                    filename + Constant.ROBOT_OUTPUT_FILE_EXTENSION);
            }

            Console.WriteLine("Output/trace files are saved in " + Constant.ROBOT_OUTPUT_FOLDER + " folder");
            Console.WriteLine("...");
            Console.WriteLine("Jobs are done! Press any key to exit...");
            Console.ReadLine();
        }
    }
}